import java.util.Scanner;

public class Smallest11 {
    public static float findSmallest(float num1,float num2,float num3){
        float smallest = num1;
        if(num2 < smallest) smallest = num2;
        if(num3 < smallest) smallest = num3;
        return smallest;
    }
     public static void main(String[] args) {
       boolean success = true;
       while (success) {
        try {
            System.out.println("***********Smallest of Three Numbers***********");
            Scanner inputTaker = new Scanner(System.in);
            System.out.println("Enter the first number");
            float num1 = inputTaker.nextFloat();
            System.out.println("Enter the second number");
            float num2 = inputTaker.nextFloat();
            System.out.println("Enter the third number");
            float num3 = inputTaker.nextFloat();
            float smallest = findSmallest(num1,num2,num3);
            System.out.println("The smallest of "+ num1+", " + num2+",and "+ num3+" is "+ smallest);
            inputTaker.close();
            success = false;
        } catch (Exception e) {
            System.out.println("Please Enter a Valid Number");
        }
       }
    }
}
