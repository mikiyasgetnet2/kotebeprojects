import java.util.Scanner;

public class ArthiOnTwoNums6 {
        public static void main(String[] args) {
            // Define the two numbers
            Scanner scanner = new Scanner(System.in);
    
            // Ask the user to enter the radius
            System.out.print("Enter the first number: ");
            double firstNumber = scanner.nextDouble();
            System.out.print("Enter the second number: ");
            double secondNumber = scanner.nextDouble();
    
            // Calculate the sum
            double sum = firstNumber + secondNumber;
            System.out.println("Sum: " + sum);
    
            // Calculate the product
            double product = firstNumber * secondNumber;
            System.out.println("Product: " + product);
    
            // Calculate the difference
            double difference = firstNumber - secondNumber;
            System.out.println("Difference: " + difference);
    
            // Calculate the division
            // We use double for division to handle non-integer results
            double division = (double) firstNumber / secondNumber;
            System.out.println("Division: " + division);
    
            // Calculate the remainder (modulo)
            double remainder = firstNumber % secondNumber;
            System.out.println("Remainder: " + remainder);
            scanner.close();
        }
    }
