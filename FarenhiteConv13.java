import java.util.Scanner;
public class FarenhiteConv13 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Ask the user to enter the temperature in Fahrenheit
        System.out.print("Enter temperature in Fahrenheit: ");
        double fahrenheit = scanner.nextDouble();

        // Convert Fahrenheit to Celsius
        double celsius = (fahrenheit - 32) * 5/9;

        // Print the result
        System.out.println("Temperature in Celsius: " + celsius);

        scanner.close();
    }
}

