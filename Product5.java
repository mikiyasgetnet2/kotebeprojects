import java.util.Scanner;
public class Product5 {
    public static void main(String[] args) {
        System.out.println("************** Product of two numbers **************");
        boolean success = true;
        while(success){
            try{
            Scanner inputTaker = new Scanner(System.in);
            System.out.print("Enter the first number: ");
            float num1 = inputTaker.nextInt();
            System.out.print("Enter the second number: ");
            float num2 = inputTaker.nextInt();
            float result = num1 * num2;
            System.out.println("The product of "+num1 +" and "+ num2 +" is "+ result);
            inputTaker.close();
            success = false;
        }catch(Exception err){
            System.out.println("Please Enter a valid number!");
        }
       }

    }
}
