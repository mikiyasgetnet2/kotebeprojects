import java.util.Scanner;

public class MultiplicationTable7 {

    public static void main(String[] args) {
        boolean success = true;
       while(success){
        try{
            Scanner scanner = new Scanner(System.in);

        // Ask user to enter a number
        System.out.print("Enter a number: ");
        int number = scanner.nextInt();

        System.out.println("Multiplication Table for " + number + ":");

        //Calculate and print the multiplication table
        for (int i = 1; i <= 10; i++) {
            int result = number * i;
            System.out.println(number + " * " + i + " = " + result);
        }
        success = false;
        scanner.close();
        }
        catch(Exception err){
            System.out.println("Please Enter a Valid Number");
        }
       }
    }
}

