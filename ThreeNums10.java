import java.util.Scanner;

public class ThreeNums10 {
    public static void main(String[] args) {
       boolean success = true;
       while (success) {
        try {
            System.out.println("***********Average of Three Numbers***********");
            Scanner inputTaker = new Scanner(System.in);
            System.out.println("Enter the first number");
            float num1 = inputTaker.nextFloat();
            System.out.println("Enter the second number");
            float num2 = inputTaker.nextFloat();
            System.out.println("Enter the third number");
            float num3 = inputTaker.nextFloat();
            float average = (num1 + num2 + num3) / 3;
            System.out.println("The average of "+ num1+", " + num2+",and "+ num3+" is "+ average);
            inputTaker.close();
            success = false;
        } catch (Exception e) {
            System.out.println("Please Enter a Valid Number");
        }
       }
    }
}
