import java.util.Scanner;

public class Factorial15 {
    public static float factorial(float num){
        if(num == 1) return num;
        return num * factorial(num - 1);
    }
    public static void main(String[] args) {
        try {
            System.out.println("***********Factorial***********");
            Scanner inputTaker = new Scanner(System.in);
            System.out.println("Enter a number");
            float num = inputTaker.nextFloat();
            float factorialResult = factorial(num);
            System.out.println(num+ "! is "+ factorialResult);
            inputTaker.close();
        } catch (Exception e) {
           System.out.println("Please Enter a valid number");
        }
    }
}
