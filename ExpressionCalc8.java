import java.util.Scanner;
public class ExpressionCalc8 {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
    
            // Ask the user to enter an expression
            System.out.print("Enter an expression: ");
            String expression = scanner.nextLine();
    
            try {
                // Using the JavaScript engine to evaluate the expression
                javax.script.ScriptEngineManager scriptEngineManager = new javax.script.ScriptEngineManager();
                javax.script.ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("JavaScript");
                Object result = scriptEngine.eval(expression);
    
                // Print the result
                System.out.println("Result: " + result);
            } catch (Exception e) {
                System.out.println("Invalid expression or error occurred: " + e.getMessage());
            }
    
            scanner.close();
        }
    }