public class ArithmeticOp4 {
    public static void main(String[] args) {
        // For a: -5 + 8 * 6
        int resultA = -5 + 8 * 6;
        System.out.println("Result of -5 + 8 * 6: " + resultA);

        // For b: (55+9) % 9
        int resultB = (55 + 9) % 9;
        System.out.println("Result of (55+9) % 9: " + resultB);

        // For c: 20 + -3*5 / 8
        double resultC = 20 + -3 * 5 / 8.0;
        System.out.println("Result of 20 + -3*5 / 8: " + resultC);

        // For d: 5 + 15 / 3 * 2 - 8 % 3
        int resultD = 5 + 15 / 3 * 2 - 8 % 3;
        System.out.println("Result of 5 + 15 / 3 * 2 - 8 % 3: " + resultD);
    }
}
