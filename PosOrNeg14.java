import java.util.Scanner;

public class PosOrNeg14 {
    public static void main(String[] args) {
         boolean success = true;
       while (success) {
        try {
            System.out.println("***********Is it Negative?***********");
            Scanner inputTaker = new Scanner(System.in);
            System.out.println("Enter a number");
            float num = inputTaker.nextFloat();
            if(num < 0) System.out.println("Negative");
            else if(num > 0) System.out.println("Positive");
            else System.out.println(" 0 is Neither Positive nor Negative");
            inputTaker.close();
            success = false;
        } catch (Exception e) {
            System.out.println("Please Enter a Valid Number");
        }
       }
    }
    }
