import java.util.Scanner;
public class Circle9 {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
    
            // Ask the user to enter the radius
            System.out.print("Enter the radius of the circle: ");
            double radius = scanner.nextDouble();
    
            // Calculate the perimeter
            double perimeter = 2 * Math.PI * radius;
    
            // Calculate the area
            double area = Math.PI * radius * radius;
    
            // Print the results
            System.out.println("Perimeter is = " + perimeter);
            System.out.println("Area is = " + area);
    
            scanner.close();
        }
    }
    
    