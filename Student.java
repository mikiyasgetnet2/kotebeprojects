import java.util.ArrayList;
import java.util.Scanner;

public class Student {
    private String name;
    private int age;
    private String address;
    private String email;

    public Student(String name, int age, String address, String email) {
        this.name = name;
        this.age = age;
        this.address = address;
        this.email = email;
    }

    // Mutator (setter) methods
    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    // Accessor (getter) methods
    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String toString() {
        return "Student{name='" + name + "', age=" + age + "}";
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Student> students = new ArrayList<>();
        for (int studentCounter = 1; studentCounter <= 5; studentCounter++) {
            try {
                System.out.println("Enter records for student number " + studentCounter);
                System.out.print("Student's Name: ");
                String name = scanner.nextLine();
                System.out.print("Student's Age: ");
                int age = scanner.nextInt();
                scanner.nextLine(); // Consume the remaining newline
                System.out.print("Student's Address: ");
                String address = scanner.nextLine();
                System.out.print("Student's Email: ");
                String email = scanner.nextLine();
                students.add(new Student(name, age, address, email));
            } catch (Exception e) {
                System.out.println("Invalid Student Record Inserted, Execution Aborted!");
                break;
            }
        }

        System.out.println("**********Students List*********");
        for (Student student : students) {
            System.out.println("Student Name: " + student.getName());
            System.out.println("Student Age: " + student.getAge());
            System.out.println("Student Address: " + student.getAddress());
            System.out.println("Student Email: " + student.getEmail());
            System.out.println(); // Empty line for separation
        }

        scanner.close();
    }
}
