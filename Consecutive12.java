import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Consecutive12 {
    public static boolean CheckConsecutiveness(float num1,float num2,float num3){
        ArrayList<Float> nums = new ArrayList<>();
        nums.add(num1);
        nums.add(num2);
        nums.add(num3);
        Collections.sort(nums);
        float middle = (num1 + num2 + num3) / 3;
        if( middle == nums.get(1)) return true;
        return false;
    }
     public static void main(String[] args) {
       boolean success = true;
       while (success) {
        try {
            System.out.println("***********Are they Consecutive Integers?***********");
            Scanner inputTaker = new Scanner(System.in);
            System.out.println("Enter the first number");
            float num1 = inputTaker.nextFloat();
            System.out.println("Enter the second number");
            float num2 = inputTaker.nextFloat();
            System.out.println("Enter the third number");
            float num3 = inputTaker.nextFloat();
            boolean AreTheyConsecutive = CheckConsecutiveness(num1,num2,num3);
            System.out.println(AreTheyConsecutive);
            inputTaker.close();
            success = false;
        } catch (Exception e) {
            System.out.println("Please Enter a Valid Number");
        }
       }
    }
}
